..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##############################################
Welcome to Squash TF documentation portal !
##############################################

.. toctree::
   :hidden:

   Squash TF components <https://squash-tf.readthedocs.io/en/latest>
   Squash Keyword Framework <https://skf.readthedocs.io/en/latest>

**Squash T**\ est **F**\ actory (aka **Squash TF**) is a set of components whose purpose is to help implement and run
the automated tests of your projects.

More precisely, our goal is :

* to be able to run your tests whatever the technology you used to implement them
* to facilitate the run of your tests on multiple environments
* to offer a solution which allows the use of multiple test stacks in one test
* to bring tools easy to integrate in CI/CD pipelines

---------------------------

**********************************************
Squash TF in the automated testing life cycle
**********************************************

A typical cycle to create an automated test has 3 phases:

* Phase 1: Specify the test to achieve
* Phase 2: Implement the automated test based on the description done in Phase 1
* Phase 3: Run the test implemented in phase 2

.. todo: add the 3 phases diagram

Let's see how Squash TF can help you in each of these phases.

The specification phase
=======================

Sorry ! Squash TF is not intended to offer help during this phase. However, its brother project **Squash T**\ est **M**\ anagement (aka **Squash TM**) can.

In addition, it is possible to establish `a bridge between Squash TF and Squash TM <https://squash-tf.readthedocs.io/projects/execution-server/en/doc-stable/tm-tf-link/tm-tf.html>`_
for the run phase.

The implementation Phase
========================

When it’s time to implement your automation tests described in phase 1, you have a wide range of tools at your disposal.

Factors that will influence your choice are :

* Your automation test’s nature (GUI test, REST API test, etc)
* Your preferred implementation language (Java, C#, python, etc)
* Your preferred scripting technique (linear, keyword-driven, etc)

For now, Squash TF is able to run, during phase 3, automation test implemented with the following tools/framework :

* `Java / Junit <https://squash-tf.readthedocs.io/projects/runner-java-junit/en/doc-stable/>`_
* `Gherkin / Cucumber (Java) <https://squash-tf.readthedocs.io/projects/runner-cucumber-java/en/doc-stable/>`_
* `Robot Framework <https://squash-tf.readthedocs.io/projects/runner-robotframework/en/doc-stable/>`_
* `Squash Keyword Framework (SKF) <https://skf.readthedocs.io/en/latest>`_
* UFT (Commercial plugin)
* Ranorex (Commercial Plugin)

The run phase
=============

The **Run** phase is supported by `Squash Execution Server <https://squash-tf.readthedocs.io/en/latest/#execution-server>`_ which will execute your tests on distributed environments and report their status to Squash TM.

It is assisted by `runners <https://squash-tf.readthedocs.io/en/latest/#runners>`_ to execute automated tests designed with the frameworks previously listed.

---------------------------

******************
Our documentations
******************

* `Squash TF components <https://squash-tf.readthedocs.io/en/latest>`_ : to learn about the components available to help you launch your automated tests.
* `Squash Keyword Framework <https://skf.readthedocs.io/en/latest>`_ : to learn about our keyword oriented automation framework.
